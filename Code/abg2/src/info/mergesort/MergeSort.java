/**
 *  @authors Laurids Bornhoeft, Lennart Ferlings, Thi Thu Hien Nguyen
 */
package info.mergesort;

import java.util.Random;

//////////////////////////
//Informatik II - SoSe 2020
//Task 2.1. a) b)
//////////////////////////

//////////////////////////
//Submission by:
//
//	Laurids Bornhoeft, Lennart Ferlings, Thi Thu Hien Nguyen
//
//////////////////////////

public class MergeSort {

//////////////////////
// Main function
	public static void main(String[] args) {
// task 2.1.a)
// Example Values:
		int[] list = { 2, 1, 6, 2, 4, 1, 6 };
		int k = 4;

// use MergeSort
		int[] newlist = kMergeSort(list, k);

// print List
		printList(newlist);

// task 2.1.b)
		analyzeTimeComplexity(7);
	}

//////////////////////
// Helper function to print the elements of a list
	private static void printList(int[] list) {
		for (int i = 0; i < list.length; i++) {
			System.out.println(list[i]);
		}
	}

//////////////////////
// Task 2.1.a)
// Implement the merge sort algorithm as stated in 
// the exercise sheet
	public static int[] kMergeSort(int[] input_list, int k) {
		if (input_list.length == 1)
			return input_list;

		int mod = input_list.length % k; // important because it is not possible to floor and ceil
		int nfrag = input_list.length / k; // how many elements per part?
		int n = 0; // global counter for current position in input_list
		int[][] parts; // save the parts the array is devided into

		if (nfrag != 0)
			parts = new int[k][];
		else
			parts = new int[mod][];

		for (int i = 0; i < parts.length; i++) { // iterate over parts
			int[] subarray;
			if (mod != 0) { // initalize the lenght of subarray in condition of modulo (that is left)
				mod--;
				subarray = new int[nfrag + 1];

			} else
				subarray = new int[nfrag];

			for (int j = 0; j < subarray.length; j++) // fill in the data from input
				subarray[j] = input_list[n++];
			parts[i] = kMergeSort(subarray, k); // save sorted subarray
			
		}

		return kMerge(parts);
	}

	private static int[] kMerge(int[][] parts) {
		int[] indices = new int[parts.length]; //save the different indices
		for (int i = 0; i < indices.length; i++) //intialize them with 0
			indices[i] = 0;

		int length = 0; // calc length of resulting list
		for (int[] part : parts)
			length += part.length; //summing up part lengthes
		int[] result = new int[length];

		//here comes the magic
		for (int i = 0; i < result.length; i++) { //iterating over the result array
			int smallest = Integer.MAX_VALUE;
			int iteratorJ = 0;
			for (int j = 0; j < parts.length; j++) {
				if (indices[j] < parts[j].length) { //if not out of bounds
					if (parts[j][indices[j]] < smallest) { //then check whether current item is smaller then the previous one
						smallest = parts[j][indices[j]];
						iteratorJ = j; //save the iterator which has to be incremented
					}
				}
			}
			indices[iteratorJ]++; //increment iterator
			result[i] = smallest; //adding the smallest item to result array
		}

		return result;
	}

//////////////////////
// Task 2.1.b)
// Create three lists of size n based on exercise 1
	private static int[] generateAverageCase(int n) {
		int[] list = new int[n];

		Random r = new Random();
		for (int i = 0; i < n; i++) {
			list[i] = r.nextInt(10000);
		}

		return list;
	}

	private static int[] generateWorstCase(int n) {
		int[] list = new int[n];

// create list in reverse order
		for (int i = 0; i < n; i++) {
			list[i] = n - (i + 1);
		}

		return list;
	}

	private static int[] generateBestCase(int n) {
		int[] list = new int[n];

// create list in order
		for (int i = 0; i < n; i++) {
			list[i] = i;
		}

		return list;
	}

//////////////////////
//Task 2.1.b)
// Apply the function from 1.2.b for every n in [1,50000]
// as well as for k = 2 and k = 7
// and measure the time
// Plot the results in an application of your choice
//
	private static void analyzeTimeComplexity(int k) { //provide the k to use as parameter
		int nMin = 1;
		int nMax = 50000;
		int step = 1000;
		
		System.out.println("n\tWorstCase\tAverageCase\tBestCase"); //table headline (column)
		for(int n = nMin; n <= nMax; n += step) { //for every n
			int[] list = generateWorstCase(n); //gen worstcase
			long worstTime = System.currentTimeMillis();
			kMergeSort(list, k); //sort worstcase
			worstTime = System.currentTimeMillis() - worstTime;
			
			list = generateAverageCase(n); //gen averagecase
			long averageTime = System.currentTimeMillis();
			kMergeSort(list, k); //sort averagecase
			averageTime = System.currentTimeMillis() - averageTime;
			
			list = generateBestCase(n); //gen bestcase
			long bestTime = System.currentTimeMillis();
			kMergeSort(list, k); //sort bestcase
			bestTime = System.currentTimeMillis() - bestTime;
			
			System.out.println(n + "\t" + worstTime + "\t" + averageTime + "\t" + bestTime); //print rows
			if(n == 1) n--; //that u dont have 1001... 2001... 3001...
		}

	}
}
